# MacOS Automated Setup

Launch `setup.sh` to automatically set up your Mac.

See the packages files under the `packages/` before launching the script to review what is going to be installed on your computer.
