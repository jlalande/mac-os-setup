#!/bin/sh

PACKAGES_DIR=./packages

echo "Starting bootstrapping"

# Check for Homebrew, install if we don't have it
if test ! $(which brew); then
  echo "Installing homebrew..."
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

brew update

echo "Installing packages..."
brew install --force $(cat $PACKAGES_DIR/brew-packages.txt)

echo "Cleaning up..."
brew cleanup

echo "Installing cask..."
brew cask

echo "Installing cask apps..."
brew cask install --force $(cat $PACKAGES_DIR/brew-cask-packages.txt)

echo "Configuring OSX..."

echo "Bootstrapping complete"
